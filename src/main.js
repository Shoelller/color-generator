let pattern='abcdef0123456789';
let hexPrefix="#"
const colorText=document.getElementById("color-text")
const colorBox=document.getElementById("color-box")
const Background=document.getElementById("bg")
const CreateColorBtn=document.getElementById("create-color")
const Copy=document.getElementById("copy")
const alertBox=document.getElementById("alert-box")
const alertText=document.getElementById("alert-text")
let color=""



CreateColorBtn.addEventListener("click",function(){
    color=""
    hexPrefix="#"
    for (let i = 0; i < 6; i++) {
        hexPrefix+=pattern[Math.floor(Math.random()*pattern.length)]
        color=hexPrefix
    }
    colorText.textContent=color
    colorBox.style.background=color
    Background.style.background=color
    Copy.style.display="flex";
})

Copy.addEventListener("click",function(){
    navigator.clipboard.writeText(color)
    alertText.textContent="Successfully copied to clipboard!"
    alertBox.classList.remove("-translate-y-48")
    alertBox.classList.add("-translate-y-20")
    setTimeout(()=>{
        alertBox.classList.remove("-translate-y-20")
        alertBox.classList.add("-translate-y-48")
        alertText.textContent=""
    },3000)
})
